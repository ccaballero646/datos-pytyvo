package com.ccaballero.datospytyvo.api.impl;

import com.ccaballero.datospytyvo.api.interfaces.IService;
import com.ccaballero.datospytyvo.jpa.interfaces.IServiceDAO;

import javax.inject.Inject;

public class Service implements IService {

    @Inject
    IServiceDAO service;


    @Override
    public String getData(String document) {
        return service.getData(document);
    }
}
