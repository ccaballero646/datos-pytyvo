package com.ccaballero.datospytyvo.api.interfaces;

public interface IService {
    String getData (String document);
}
