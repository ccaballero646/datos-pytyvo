package com.ccaballero.datospytyvo.web.binder;

import com.ccaballero.datospytyvo.api.impl.Service;
import com.ccaballero.datospytyvo.api.interfaces.IService;
import com.ccaballero.datospytyvo.jpa.impl.ServiceDAO;
import com.ccaballero.datospytyvo.jpa.interfaces.IServiceDAO;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

public class Binder extends AbstractBinder {
    @Override
    protected void configure() {
        bind(ServiceDAO.class).to(IServiceDAO.class);
        bind(Service.class).to(IService.class);

    }
}
