package com.ccaballero.datospytyvo.web.rs;

import com.ccaballero.datospytyvo.api.interfaces.IService;
import com.ccaballero.datospytyvo.web.utils.ResourceThreadFactory;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Path("data")
public class Resource {


    @Inject
    IService service;

    private static final int CORE_POOL_SIZE = Integer.parseInt(System.getProperty("core.pool.size"));

    private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(CORE_POOL_SIZE,
            Integer.MAX_VALUE, 60L,
            TimeUnit.SECONDS, new SynchronousQueue<>(true), new ResourceThreadFactory("executor"));

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public void getData(
            @Suspended final AsyncResponse asyncResponse,
            @QueryParam("cedula") String document) {
        EXECUTOR.submit(() -> asyncResponse.resume(doGetData(document)));
    }



    private Response doGetData(String document){
        return Response.ok(service.getData(document)).build();
    }
}
