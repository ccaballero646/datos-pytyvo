package com.ccaballero.datospytyvo.web.application;

import com.ccaballero.datospytyvo.web.binder.Binder;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/v1")
public class RestApp extends ResourceConfig {

    public RestApp() {
        register(new Binder());
        packages(false, "com.ccaballero.datospytyvo.web.rs");
    }


}



