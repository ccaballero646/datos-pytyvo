package com.ccaballero.datospytyvo.jpa.interfaces;

public interface IServiceDAO {

    String getData(String document);
}
