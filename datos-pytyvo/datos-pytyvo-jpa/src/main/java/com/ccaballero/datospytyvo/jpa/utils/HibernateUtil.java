package com.ccaballero.datospytyvo.jpa.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class HibernateUtil {

    protected final SessionFactory sessionFactory = HibernateSessionFactory.getSessionFactory();

    public Session openSession() {
        return sessionFactory.openSession();
    }
}
