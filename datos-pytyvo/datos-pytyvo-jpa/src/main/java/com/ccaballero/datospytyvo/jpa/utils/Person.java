package com.ccaballero.datospytyvo.jpa.utils;

public class Person {

    public Person(String document, String name, String city, String district) {
        this.document = document;
        this.fullName = name;
        this.city = city;
        this.district = district;
    }


    private static final String NEWLINE = System.lineSeparator();
    private static final String FORMAT = "Cedula: %s "
            + NEWLINE + "Nombre y Apellido: %s"
            + NEWLINE + "Ciudad: %s"
            + NEWLINE + "Distrito: %s";
    private String document;
    private String fullName;
    private String city;
    private String district;

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String toString() {
        return String.format(FORMAT, document, fullName, city, district);
    }
}
