package com.ccaballero.datospytyvo.jpa.impl;

import com.ccaballero.datospytyvo.jpa.interfaces.IServiceDAO;
import com.ccaballero.datospytyvo.jpa.utils.HibernateUtil;
import com.ccaballero.datospytyvo.jpa.utils.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.NoResultException;

public class ServiceDAO extends HibernateUtil implements IServiceDAO {

    private static final String QUERY = "select document, name, city, district from data where document = :document";
    private static final String NO_RESULT_MSG = "No se encontraron datos para el documento %s";
    private static final String ERROR_MSG = "Hubo un error al consultar. Favor intentelo mas tarde";
    private static final Logger LOGGER = LogManager.getLogger(ServiceDAO.class);
    @Override
    public String getData(String document) {

        Query query;
        try (Session s = openSession()) {
            s.getTransaction().begin();
            query = s.createSQLQuery(QUERY);
            query.setParameter("document", document);
            query.setMaxResults(1);


            Object[] result = (Object[]) query.getSingleResult();
            String doc = (String)result[0];
            String name = (String)result[1];
            String city = (String)result[2];
            String district = (String)result[3];
            s.getTransaction().commit();
            Person p = new Person(doc, name, city, district);
            return p.toString();

        }
        catch (NoResultException e) {
            LOGGER.warn("No result");
            return String.format(NO_RESULT_MSG, document);
        }
        catch (Exception e) {
            LOGGER.error("Error executing query", e);
            return ERROR_MSG;
        }
    }

}
