package com.ccaballero.datospytyvo.jpa.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.io.File;

public class HibernateSessionFactory {

    private static SessionFactory sessionFactory;
    private static final Object lock = new Object();
    private static final Logger LOGGER = LogManager.getLogger(HibernateSessionFactory.class);


    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            synchronized (lock) {
                try {
                    String hibernateCfg = System.getProperty("hibernate-app");
                    StandardServiceRegistry registry;
                    if (hibernateCfg == null) {
                        registry = (new StandardServiceRegistryBuilder()).configure("hibernate-app.cfg.xml").build();
                        sessionFactory = (new MetadataSources(registry)).buildMetadata().buildSessionFactory();
                    } else {
                        registry = (new StandardServiceRegistryBuilder()).configure(new File(hibernateCfg)).build();
                        sessionFactory = (new MetadataSources(registry)).buildMetadata().buildSessionFactory();
                    }
                } catch (HibernateException var3) {
                    LOGGER.error("Ocurrió un error en la inicialización de la SessionFactory", var3);
                    throw new ExceptionInInitializerError(var3);
                }
            }
        }

        return sessionFactory;
    }

}
