#!/bin/sh

export DEBUG_PORT="8000"

CATALINA_OPTS=" -server"

CATALINA_OPTS="$CATALINA_OPTS -Dcore.pool.size=10"

# MEMORY CONFIG
CATALINA_OPTS="$CATALINA_OPTS -Xms64m"
CATALINA_OPTS="$CATALINA_OPTS -Xmx128m"

# JVM CONFIG
CATALINA_OPTS="$CATALINA_OPTS -Duser.timezone=America/Asuncion"
CATALINA_OPTS="$CATALINA_OPTS -XX:+UnlockDiagnosticVMOptions"
CATALINA_OPTS="$CATALINA_OPTS -XX:+LogVMOutput"
CATALINA_OPTS="$CATALINA_OPTS -agentlib:jdwp=transport=dt_socket,address=*:$DEBUG_PORT,server=y,suspend=n"


###############         G1              ###############
CATALINA_OPTS="$CATALINA_OPTS -XX:+UseG1GC"
CATALINA_OPTS="$CATALINA_OPTS -XX:MaxGCPauseMillis=300"                      # Hints to the G1 collector how long pauses should be; the G1 algotithm is adjusted to attempt to meet that goal